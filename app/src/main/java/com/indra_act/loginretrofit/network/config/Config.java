package com.indra_act.loginretrofit.network.config;

public class Config {

    public static final String BASE_URL = "http://192.168.1.99";

    public static final String API_URL = BASE_URL + "/login_koreanday";

    public static final String API_LOGIN = API_URL + "/login.php";
    public static final String API_REGISTER = API_URL + "/register.php";

}